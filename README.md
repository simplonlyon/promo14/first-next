# Projet Next.js fullstack

Petit projet Next.js pour apprendre à utiliser les différents composants de celui ci, pages et api.

**/!\Attention** Il a été fait le choix de mettre du typeorm et de gérer toute l'API sur Next.js directement. Pédagogiquement c'est pas inintéressant, pasque ça fait travailler les routes et les api de Next, mais en vrai, c'est probablement une assez mauvaise idée, et même dans le cas où on est sur une stack JS fullstack, il serait préférable de faire un projet back avec express/typeorm et un projet front avec Next.js et juste le react dessus sans API

## How To use
1. Cloner et `npm install`
2. Créer une base de données (`next_db` si vous voulez qu'elle s'appelle comme dans le .env actuel)
3. Ajouter un `.env.local` dans lequel il faudra indiquer :
```
GITLAB_CLIENT_ID=votre_cle_appli_gitlab
GITLAB_CLIENT_SECRET=votre_secret_appli_gitlab
EMAIL_SERVER=votre_server_smtp
EMAIL_FROM=email_expediteur
```
4. Lancer `npm run dev`