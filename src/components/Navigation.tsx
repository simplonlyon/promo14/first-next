import { Container, Navbar, Nav } from "react-bootstrap";
import Link from 'next/link';
import { signIn, signOut, useSession } from "next-auth/client";


export function Navigation() {
    const [session] = useSession();

    return (
        <Navbar bg="light" expand="lg">
            <Container>

                <Link href="/" passHref>
                    <Navbar.Brand>First Next</Navbar.Brand>
                </Link>
                <Navbar.Toggle aria-controls="basic-navbar-nav" />
                <Navbar.Collapse id="basic-navbar-nav">
                    <Nav className="me-auto">
                        <Link href="/" passHref>
                            <Nav.Link>
                                Home
                            </Nav.Link>
                        </Link>
                        <Link href="/dog" passHref>
                            <Nav.Link>
                                Dogs
                            </Nav.Link>
                        </Link>
                        <Link href="/first" passHref>
                            <Nav.Link>
                                First
                            </Nav.Link>
                        </Link>
                        {session ?
                            <>
                                <Link href="/protected" passHref>
                                    <Nav.Link>
                                        Protected
                                    </Nav.Link>
                                </Link>
                                <Nav.Link onClick={() => signOut()}>
                                    Sign Out
                                </Nav.Link>
                            </>
                            :
                            <Nav.Link onClick={() => signIn()}>
                                Sign In
                            </Nav.Link>
                        }

                    </Nav>
                </Navbar.Collapse>
            </Container>
        </Navbar>
    );
}