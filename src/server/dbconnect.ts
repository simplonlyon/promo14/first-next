import { createConnection, getConnection } from "typeorm";
import { Dog } from "./entities/Dog";

export async function dbConnect() {
    try {
        return getConnection();
    } catch(e) {
        return createConnection({
            type: 'mysql',
            url: process.env.DATABASE_URL,
            synchronize: true,
            entities: [Dog]
        });
    }
}