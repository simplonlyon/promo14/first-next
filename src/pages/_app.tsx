import 'bootstrap/dist/css/bootstrap.min.css';
import '../styles/globals.css'
import type { AppProps } from 'next/app'
import axios from 'axios'
import { Navigation } from '../components/Navigation';
import { Provider } from 'next-auth/client';

axios.defaults.baseURL = 'http://localhost:3000'

function MyApp({ Component, pageProps }: AppProps) {
  return (
    <Provider session={pageProps.session}>
      <Navigation></Navigation>
      <Component {...pageProps} />
    </Provider>
  );
}
export default MyApp
