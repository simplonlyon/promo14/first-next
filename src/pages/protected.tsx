import { NextPage } from "next";
import { signIn, useSession } from "next-auth/client";

const Protected: NextPage = () => {
    const [session, loading] = useSession()

    return (
        <div>
            {session && <p>Coucou {session.user.email}</p>}
            {!session && <button onClick={() => signIn()}>Sign in</button>}
        </div>
    );

};

export default Protected;