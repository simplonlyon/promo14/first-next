import { NextPage } from "next";
import Head from 'next/head';


const First: NextPage = () => {

    return (
        <div>
            <Head>
                <title>My First Page</title>
            </Head>
            <p>Hello from first page</p>
        </div>
    );
};

export default First;