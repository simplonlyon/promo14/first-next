import NextAuth from "next-auth";
import Providers from "next-auth/providers";


export default NextAuth({
    providers: [
        Providers.GitLab({
            clientId: process.env.GITLAB_CLIENT_ID,
            clientSecret:process.env.GITLAB_CLIENT_SECRET
        }),
        Providers.Email({
            server: process.env.EMAIL_SERVER,
            from: process.env.EMAIL_FROM
        })
    ],
    database: process.env.DATABASE_URL+"?synchronize=true"
})