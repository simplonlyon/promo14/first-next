import { NextApiRequest, NextApiResponse } from "next";
import { dbConnect } from "../../../server/dbconnect";
import { Dog } from "../../../server/entities/Dog";




export default async function handler(
    req:NextApiRequest, 
    res:NextApiResponse<Dog>) {

    const connection = await dbConnect();
    
    const dog = await connection.getRepository<Dog>('Dog').findOne(Number(req.query.id));

    if(!dog) {
        return res.status(404).end('Not found');
    }
    res.json(dog);
}