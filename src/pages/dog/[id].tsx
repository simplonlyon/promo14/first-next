import axios from "axios";
import { GetServerSideProps, GetStaticPaths, GetStaticProps, NextPage } from "next";
import { Dog } from "../../server/entities/Dog";



interface Props {
    dog: Dog;
}

const OneDog: NextPage<Props> = ({ dog }) => {

    return (
        <p>{dog.name} {dog.breed} {dog.age}</p>
    )
}

export default OneDog;


export const getStaticPaths: GetStaticPaths = async () => {
    const response = await axios.get<Dog[]>('/api/dog')

    return {
        paths: response.data.map(item => ({ params: { id: ''+item.id } })),
        fallback: false
    }
}

export const getStaticProps: GetStaticProps<Props> = async (context) => {
    try {
        const response = await axios.get('/api/dog/' + context.params?.id);

        return {
            props: {
                dog: response.data
            }
        }
    } catch (e) {
        return {
            notFound: true
        }
    }
}