import { GetStaticProps, NextPage } from "next";
import Link from 'next/link';
import axios from 'axios'
import { Dog } from "../../server/entities/Dog";

const DogPage: NextPage<{ dogs: Dog[] }> = ({ dogs }) => {

    return (
        <div>
            <h1>Dog List</h1>

            <ul>
                {dogs.map(dog =>
                    <li key={dog.id}>
                        <Link href={"/dog/" + dog.id}>{dog.name}</Link>
                    </li>
                )}
            </ul>
        </div>
    )
}

export default DogPage;

export const getStaticProps: GetStaticProps = async () => {

    const response = await axios.get('/api/dog');

    return {
        props: {
            dogs: response.data

        }
    }
}